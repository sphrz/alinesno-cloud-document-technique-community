## 服务工程的命名规范

<p class="show-images"><img src="/images/undraw_operating_system_4lr6.svg" width="40%" /></p>

### 工程命名规范
- 工程结构清晰，统一采用`alinesno+工程性质+业务`命名，中间采用"-"连接，如
    ```
    alinesno-base-logs //基础日志工程
    ```
- 统一采用英文命名，禁止使用拼音代表含义，每个英文单词不超过10个字母；
- 工程命名统一采用小写字母，禁止使用大字字母；

### 工程包的命名规范
- 工程包命名统一采用小写字母，禁止使用大字字母；
- 工程包统一使用前缀`com.alinesno.cloud`；
- 工程包命名规范定义为`前缀+工程性质+业务`,服务名称单词连接使用"."代替"-",例如：
    ```
    com.alinesno.cloud.base.logs        // 基础日志包
    com.alinesno.cloud.business.cms     // cms业务服务包
    ```
- 工程模块包命名规范定义如下,以消息服务(alinesno-base-logs)为例子：
    ```
    com.alinesno.cloud.base.logs.entity      // 持久层对象包
    com.alinesno.cloud.base.logs.repository  // 持久层包
    com.alinesno.cloud.base.logs.bean        // 实体类包
    com.alinesno.cloud.base.logs.service     // 服务包
    com.alinesno.cloud.base.logs.emnus       // 枚举包
    com.alinesno.cloud.base.logs.constants   // 常量包
    com.alinesno.cloud.base.logs.utils       // 工具包
    com.alinesno.cloud.base.logs.exception   // 异常包
    ```
- 每个服务工程下必须添加`README.md`文件作为工程说明.

### 实体类规范
- 类命名统一驼峰形式，禁止使用中文拼音缩写，每个单词长度不能超过10个字母；
- 类命名统一使用模块名称做为后缀，如`LogsErrorEntity`,则为持久层对象；
- 类在每个包模块包命名规范定义如下,以消息服务(alinesno-base-message)为例子：
    ```
    com.alinesno.cloud.base.logs.entity.LogsErrorEntity                   // 持久层对象以`Entity`结尾
    com.alinesno.cloud.base.logs.repository.LogsErrorRepository           // 持久层操作以`Repository`结尾
    com.alinesno.cloud.base.logs.repository.impl.LogsErrorRepositoryImpl  // 持久层实现操作以`RepositoryImpl`结尾
    com.alinesno.cloud.base.logs.bean.LogsErrorBean                       // 实体类对象以`Bean`结尾
    com.alinesno.cloud.base.logs.service.LogsErrorService                 // 服务以`Service`结尾
    com.alinesno.cloud.base.logs.service.impl.LogsErrorServiceImpl        // 服务实现以`ServiceImpl`结尾
    com.alinesno.cloud.base.logs.emnus.LogsErrorEmnus                     // 枚举对象只放在Emnus对象中，工程中唯一枚举对象，以`工程名+Emnus` 命名
    com.alinesno.cloud.base.logs.constants.LogsErrorConstants             // 常量只放在一个constants对象中，工程中唯一常量对象,以`工程名+Constants`命名
    com.alinesno.cloud.base.logs.comstants.LogsConstants                  // 自定义异常类以`工程名+异常性质+Exception`命名
    com.alinesno.cloud.base.logs.utils.LogsErrorUtils                     // 工具只放在utils对象中，工程中唯一工具对象，以`工程名+Utils`命名
    com.alinesno.cloud.base.logs.exception.LogsErrorException             // 自定义异常类以`工程名+异常性质+Exception`命名
    com.alinesno.cloud.base.logs.exception.LogsException                  // 自定义全局异常类以`工程名+异常性质+Exception`命名
    ```
- 枚举对象(emnus)、常量对象(constants)和工具对象(utils)包含有能用方法需做提取出来放于核心工程包中。

### 基类规范
- 持久层对象继承基类`BaseEntity`,例如:
    ```
    class LogsErrorEntity extends BaseEntity{}
    ```
- 持久层操作继承基类`IBaseRepository`,例如:
    ```
    interface LogsErrorRepository extends IBaseRepository<DemoEntitLogsErrorEntity , String>
    ```
- 实体类对象继承基类`BaseBean`,例如 ：
    ```
    class LogsErrorBean extends BaseBean{}
    ```
- 业务服务类继承基类`BaseService`,例如:
    ```
    class LogsErrorService extends BaseService{}
    ```
- 异常类继承基类`BaseException`,例如:
    ```
    class LogsErrorException extends BaseException{}
    ```


- 每个工程的启动类统一放置于`工程包`下，类名统一名称`alinesnoApplication`:
    ```
    com.alinesno.cloud.base.logs.LinesnoApplication // 基础日志包启动类
    ```

